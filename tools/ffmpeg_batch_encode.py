#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
"""
Merge files from 2 directories into a third one using mkvmerge
"""

import os
import io
import time
import argparse
from subprocess import Popen, PIPE


# -----------------------------------------------------------------------------
# get_file_info
# -----------------------------------------------------------------------------
def get_file_info(in_file, ffmpeg_path):
    print(handle_ffmpeg(in_file, None, ffmpeg_path, None))
    # TODO


# -----------------------------------------------------------------------------
# list_files_in_dir
# -----------------------------------------------------------------------------
def list_files_in_dir(root_path, filetypes):
    '''
    List files in directory and subdirectory of path and return them in a list
    root_path is a string of the path to walk into
    filetypes is a list of the files filetypes (without dot)
    '''
    # Do some check on arguments
    if (root_path is None or
            filetypes is None or
            not os.path.isdir(root_path) or
            not isinstance(filetypes, (list, tuple))):
        print('ERROR: list_files_in_dir: Arguments provided not coherent')
        return -1

    # Format strings
    filetypes = [i if not i.startswith('.') else i[1:] for i in filetypes]

    total_list = []
    for path, subdirs, files in os.walk(root_path, topdown=True):
        for name in files:
            if os.path.splitext(name)[1][1:] in filetypes:
                total_list.append(os.path.join(path, name))
    return total_list


# -----------------------------------------------------------------------------
# true_or_false
# -----------------------------------------------------------------------------
def true_or_false(arg):
    '''
    Converts string into boolean
    '''
    # Do some check on arguments
    if arg is not str:
        print('ERROR: true_or_false : unexpected data type')
        return -1
    value = str(arg).upper()
    if value == 'FALSE'[:len(value)]:
        return False
    else:
        return True


# -----------------------------------------------------------------------------
# handle_ffmpeg
# -----------------------------------------------------------------------------
def handle_ffmpeg(in_file, out_file, ffmpeg_path, params):
    '''
    Handles the ffmpeg call by creating the call string
    Must provide:
    - in_file : input file complete path string
    - out_file : output file complete path string
    - ffmpeg_path : ffmpeg binary path string (uses system if set to None)
    Optional:
    - params : list containing ffmpeg parameters and value (without dash)
    '''
    # Check parameters
    if not isinstance(in_file, str) or not isinstance(out_file, str):
        print('Error, missing parameter')
        return False
    if not os.path.isfile(os.path.abspath(in_file)):
        print('The input file doesn\'t exist !')
        return False
    if ffmpeg_path is not None:
        if not isinstance(ffmpeg_path, str) or not os.path.isfile(ffmpeg_path):
            print('The ffmpeg path seems not coherent')
            return False
    if ffmpeg_path is None:
        soft = 'ffmpeg'
    elif os.path.isfile(os.path.abspath(ffmpeg_path)):
        soft = os.path.join(ffmpeg_path)
    else:
        print('ffmpeg isn\'t in the path provided !')
        return False
    if params is not None:
        if not isinstance(params, (list, tuple)):
            print('The params are not in a list !')
            return False

    # Create encode string
    command = []
    command.append(soft)
    # Input file params

    command.append('-i')
    command.append('\'%s\'' % in_file)
    # Set output file params
    # We assume that the even numbers in list are the command and odd the value
    if params is not None:
        for num, param in enumerate(params):
            if num % 2:
                command.append(param)
            else:
                parameter = '-' + str(param)
                command.append(parameter)

    if out_file is not None:
        command.append('\'%s\'' % out_file)

    # Execute ffmpeg
    code = False
    print('Encoding %s' % in_file)
    command = ' '.join(command)
    print('Encode string: %s' % command)
    if out_file[-1:] == '/':
        out_file = out_file[:-1]
    fname = out_file + '.log'

    with io.open(fname, 'wb') as writer, io.open(fname, 'rb', 1) as reader:
        process = Popen(command,
                        shell=True,
                        stdin=PIPE,
                        stdout=writer,
                        stderr=writer)
        while process.poll() is None:
            # FFmpeg crashed or asks something...
            out = reader.read()
            if len(out) > 0:
                print(out)
                if 'already exists. Overwrite ? [y/N]' in str(out):
                    process.stdin.write(b'y\n')
            time.sleep(0.5)

    return 0


# -----------------------------------------------------------------------------
# batch_encode
# -----------------------------------------------------------------------------
def batch_encode(in_path, out_path, ffmpeg_path, filetypes, params):
    '''
    Launch encode of files in in_path folder and save them to out_path
    Must provide:
    - in_path : input files complete path string
    - out_file : output complete path string
    - ffmpeg_path : ffmpeg binary path string (uses system if set to None)
    - filetypes : list of filetype to be taken in account for encoding
    Optional:
    - params : list containing ffmpeg parameter and value
    '''
    # Check parameters
    if not isinstance(in_path, str) or not isinstance(out_path, str):
        print('Error, missing parameter')
        return False
    if not os.path.isdir(os.path.abspath(in_path)):
        print('The input file doesn\'t exist !')
        return False
    if not isinstance(filetypes, (list, tuple)):
        # Check if we only have one parameter and put in a list
        if isinstance(filetypes, str) and ' ' not in filetypes:
            filetypes = (filetypes,)
        else:
            print('The filetypes are not in a list !')
            return False
    if params is not None:
        if not isinstance(params, (list, tuple)):
            print('The params are not in a list !')
            return False
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    outstring = []

    in_path_list = list_files_in_dir(os.path.abspath(in_path), filetypes)
    # Create output directory if it doesn't exist
    if not os.path.isdir(out_path):
        os.makedirs(out_path)

    # Take video from files in in_path_list
    for in_file in in_path_list:
        encode_result = False
        # Get path and filename
        path_in, file_in = os.path.split(os.path.abspath(in_file))
        # Create target file string
        out_file = os.path.join(os.path.abspath(out_path), file_in)
        # Call ffmpeg
        encode_result = handle_ffmpeg(in_file, out_file, ffmpeg_path, params)
        if encode_result:
            outstring.append('Processed: %s' % in_file)
        else:
            outstring.append('ERROR processing: %s' % in_file)
    return '\n'.join(outstring)


# -----------------------------------------------------------------------------
# main
# -----------------------------------------------------------------------------
def main():
    '''
    Main function
    To have a list of ffmpeg x264 parameters go to:
    https://sites.google.com/site/linuxencoding/x264-ffmpeg-mapping
    '''

    parser = argparse.ArgumentParser(description='Process arguments')
    parser.add_argument('-i', '--in', dest='in_path',
                        type=str, required=True,
                        default=None, help='path to videos directory')
    parser.add_argument('-e', '--ext', dest='extensions',
                        type=str, required=True,
                        default=None, nargs='+',
                        help='extensions of files separated by spaces')
    parser.add_argument('-o', '--out', dest='out_path',
                        type=str, required=False,
                        default=None, help='path to outputs')
    parser.add_argument('-f', '--ffmpeg', dest='ffmpeg_path',
                        type=str, required=False,
                        default=None, help='path to custom ffmpeg')
    parser.add_argument('-p', '--parameters', required=False,
                        type=str, dest='params', default=None, nargs='+',
                        help='params for ffmpeg (default: % (default)s)')
    args = vars(parser.parse_args())
    # Avoid 'astpskip' when empty parameter
    if args['ffmpeg_path'] == 'astpskip':
        args['ffmpeg_path'] = None
    if args['params'] == 'astpskip':
        args['params'] = None
    output = batch_encode(args['in_path'],
                          args['out_path'],
                          args['ffmpeg_path'],
                          args['extensions'],
                          args['params'])
    print('================================')
    print(output)
    # TODO: handle arguments with dash (ex: flags2 -fastpskip)


if __name__ == '__main__':
    main()
