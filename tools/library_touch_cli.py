#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
"""
Change the creation date of files based on folder date and EXIF data
Usable from command line.
"""

import os
import argparse
import logging
import coloredlogs
import re
import json
import csv
import platform
import shlex
from subprocess import check_output
from datetime import datetime, timezone

import piexif

from hachoir.parser import createParser
from hachoir.metadata import extractMetadata

from termcolor import cprint

from enum import Enum


# Create a logger object.
logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG', logger=logger)
logger.setLevel(logging.INFO)


# Some parameters
OS_NAME = platform.system()
EXIF_TAKE_TIME_ORIG = 36867
MARGIN_TOUCH = 91   # Consider file stamp diff lower than this to be ok
MARGIN_EXIF = 182   # Consider exif stamp diff lower than this to be ok
MARGIN_DECISION = 365   # Difference between metadata and parent folder
EXIF_EXT = ['.jpg', '.jpeg']
SUPPORTED_EXT = ['.png', '.mp4', '.mpeg', '.mpg',
                 '.mts', '.3gp', '.mov', '.mkv', '.asf']


class file_attribute(object):
    """
    File information class. Contains file path, type and fathered information
    """
    def __init__(self, p: str, file_stamp: datetime = None,
                 metadata_stamp: datetime = None,
                 parent_stamp: datetime = None):
        absp = os.path.abspath(p)
        assert os.path.exists(absp)
        _, file_extension = os.path.splitext(absp)

        self.path = absp
        self.extension = file_extension.lower()
        self.type = 'dir' if os.path.isdir(absp) else 'file'
        self.file_stamp = file_stamp
        self.metadata_stamp = metadata_stamp
        self.parent_stamp = parent_stamp
        self.decided_stamp = None
        self.action = Action.SKIP

    def __str__(self) -> str:
        s = '{} ({}): Do {} apply:{} (f:{}, m:{}, p:{})'.format(
                self.path, self.type, self.action, self.decided_stamp,
                self.file_stamp, self.metadata_stamp, self.parent_stamp)
        return s


class Action(Enum):
    SKIP = 0
    CHANGE_METADATA = 1
    CHANGE_FILESTAMP = 2
    CHANGE_ALL = 3


def query_yes_no(question: str, default: str = 'no'):
    """
    Ask a yes/no question via raw_input() and return their answer.

    'question' is a string that is presented to the user.
    'default' is the presumed answer if the user just hits <Enter>.
        It must be 'yes' (the default), 'no' or None (meaning
        an answer is required of the user).

    The 'answer' return value is True for 'yes' or False for 'no'.
    @params:
        question - Required : question to show to the user (str)
        dafault  - Optional : default answer if pressing return (str)
    @returns:
        (bool) user response to the question
    """
    valid = {'yes': True, 'y': True, 'ye': True,
             'no': False, 'n': False}
    if default is None:
        prompt = ' [y/n] '
    elif default == 'yes':
        prompt = ' [Y/n] '
    elif default == 'no':
        prompt = ' [y/N] '
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        cprint(question + prompt, 'blue')
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            cprint("Please respond with 'yes' or 'no' (or 'y' or 'n').\n",
                   'red')


# Print iterations progress, see:
# https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
def print_progressbar(iteration: int, total: int,
                      prefix: str = '', suffix: str = '', decimals: int = 1,
                      length: int = 80, fill: str = '█', color: str = 'white'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (int)
        total       - Required  : total iterations (int)
        prefix      - Optional  : prefix string (str)
        suffix      - Optional  : suffix string (str)
        decimals    - Optional  : number of decimals in percent complete (int)
        length      - Optional  : character length of bar (int)
        fill        - Optional  : bar fill character (str)
        color       - Optional  : bar color (str)
    @returns:
        nothing
    """
    percent = ("{0:." + str(decimals) + "f}").format(
               100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    cprint('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix),
           color, end='\r')
    # Print New Line on Complete
    if iteration == total:
        cprint('')


def align_exif_date_with_decided(obj: file_attribute):
    """
    Changes the EXIF creation date to be aligned with the decided change
    @params:
        obj - Required : file attribute
    @returns:
        nothing
    """
    assert isinstance(obj, file_attribute)
    assert (obj.action == Action.CHANGE_METADATA or
            obj.action == Action.CHANGE_ALL)  # Extra checks are seldom bad

    # Get original exif data
    try:
        exif_dict = piexif.load(obj.path)
    except Exception as e:
        logger.debug('No exif data for {}: {}'.format(obj.path, e))
        return

    # Change creation date in exif_dict
    date = obj.decided_stamp.strftime('%Y:%m:%d %H:%M:%S').encode('ascii')
    try:
        exif_dict['Exif'][EXIF_TAKE_TIME_ORIG] = date
    except (KeyError, piexif._exceptions.InvalidImageDataError):
        return
    try:
        exif_bytes = piexif.dump(exif_dict)
        piexif.insert(exif_bytes, obj.path)
    except Exception as e:
        logger.warning('Got: {} while processing {}'.format(e, obj.path))
    return


def align_file_date_with_decided(obj: file_attribute):
    """
    Changes the EXIF creation date to be aligned with the decided change
    @params:
        obj - Required : file attribute
    @returns:
        nothing
    """
    assert isinstance(obj, file_attribute)
    assert (obj.action == Action.CHANGE_FILESTAMP or
            obj.action == Action.CHANGE_ALL)  # Extra checks are seldom bad

    file_t = obj.decided_stamp.strftime('\'%m/%d/%Y %H:%M:%S\'')

    logger.debug('Changing file date of {} to {} (platform:{})'.format(
                 obj.path, file_t, OS_NAME))

    if OS_NAME == 'Linux':
        # Datetime to UTC timestamp
        timestamp = obj.decided_stamp.replace(tzinfo=timezone.utc).timestamp()
        os.utime(obj.path, (timestamp, timestamp))
    elif OS_NAME == 'Darwin':
        # Use SetFile to change creation and modification date
        op = shlex.quote(obj.path)
        cmd = ' '.join(['SetFile', '-d', file_t, '-m', file_t, op])
        try:
            check_output(cmd, shell=True)   # Use shell to avoid error 2
        except Exception as e:
            logger.debug('Error {} on {} using {}'.format(e, obj.path, cmd))
    else:
        logging.warning('Platform {} not supported'.format(OS_NAME))


def get_objects_from_path(path: str, extensions: list = None) -> list:
    """
    Get a list of files in the specified path
    @params:
        path       - Required : path to the top folder to list from (str)
        extensions - Optional : list with extensions to consider (list[str])
    @returns:
        (list[file_atribute]) List of the objects in the parent folder
    """
    assert os.path.exists(path)
    assert extensions is None or isinstance(extensions, list)
    assert not os.path.isfile(path)
    path = os.path.abspath(path)

    # Get directories to determine os.walk progress
    total = len(list(os.walk(path)))

    # Get file list
    objects = []
    cnt = 0
    for directory, _, file_list in os.walk(path):
        # Print progress
        cnt += 1
        print_progressbar(cnt, total, prefix='1. List files')

        # Walk through
        directory = os.path.abspath(directory)
        objects.append(file_attribute(directory))
        for f in file_list:
            f = os.path.abspath(os.path.join(path, directory, f))
            objects.append(file_attribute(f))
    return objects


def datetime_from_string(txt: str) -> (datetime, None):
    """
    Converts a string to datetime.
    String should be of the form 'year-month-day ...'. All should be integers
     @params:
        txt - Required : string containing the date (str)
    @returns:
        (datetime) date in the text or None if invalid
    """
    assert isinstance(txt, str)
    default_missing = '01'

    p = re.compile('(\d+)')
    found = re.findall(p, txt)
    # Extend the list to make sure we have 3 values at least
    found = found + [0 for _ in range(3 - len(found))]
    # Validate and move to date
    date = [found[0] if (int(found[0]) >= 1900 and
                         int(found[0]) <= 2100) else None]               # year
    date.append(found[1] if (int(found[1]) >= 1 and
                             int(found[1]) <= 12) else default_missing)  # mon
    date.append(found[2] if (int(found[2]) >= 1 and
                             int(found[2]) <= 31) else default_missing)  # day
    # Convert to datetime object
    try:
        date = datetime.strptime('-'.join(date), '%Y-%m-%d')
        logger.debug('Found date {} from parent folder {}'.format(
                     date.strftime('%Y-%m-%d'), txt))
    except (ValueError, TypeError):  # Not a valid date
        logger.debug('invalid date:{} for parent folder {}'.format(date, txt))
        return None
    return date


def get_parent_time(obj: file_attribute) -> (datetime, None):
    """
    Returns datetime based parent folder written date
    Parent folder name should be '2018 ...', '2018-06 ...' or '2018-06-23 ...'
    2018 being the year, 06 month and 23 the day of pictures creation.
     @params:
        obj - Required : object to consider (file_atribute)
    @returns:
        (datetime) date of the parent or None if invalid
    """
    assert isinstance(obj, file_attribute)

    if not os.path.isfile(obj.path):    # Only process files
        return None
    parent = os.path.dirname(obj.path)
    assert os.path.isdir(parent)
    parent = parent.split(os.sep)[-1]

    # Determine parent date from name
    date = datetime_from_string(parent)

    return date


def get_object_time(obj: file_attribute) -> (datetime, None):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible. Returns a datetime.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    @params:
        obj - Required : object to consider (file_atribute)
    @returns:
        (datetime) creation date from the OS or None if invalid
    """
    assert isinstance(obj, file_attribute)

    if not os.path.isfile(obj.path):    # Only process files
        return None

    found = None

    if OS_NAME == 'Windows':
        found = os.path.getctime(obj.path)
    else:
        stat = os.stat(obj.path)
        try:
            found = stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            found = stat.st_mtime

    date_created = datetime.fromtimestamp(found)
    logger.debug('Found date {} from file {}'.format(
                 date_created.strftime('%Y-%m-%d'), obj.path))
    return date_created


def get_exif_time(obj: file_attribute) -> (datetime, None):
    """
    Returns the datetime from the exif data (JPG) creation date
    @params:
        obj - Required : object to consider (file_atribute)
    @returns:
        (datetime) exif creation date or None if invalid
    """
    assert isinstance(obj, file_attribute)

    if not os.path.isfile(obj.path):    # Only process files
        return None

    # Extract the metadata (if available)
    try:
        exif_dict = piexif.load(obj.path)
        d = exif_dict['Exif'][EXIF_TAKE_TIME_ORIG]
    except Exception as e:
        logger.debug('piexif error on {}: {}'.format(obj.path, e))
        return None

    # Convert to datetime object
    try:
        date = datetime.strptime(d.decode('ascii'), '%Y:%m:%d %H:%M:%S')
        logger.debug('Found date {} from exif data {} (file:{})'.format(
                     date.strftime('%Y-%m-%d'), d, obj.path))
    except (ValueError, TypeError):  # Not a valid date
        logger.debug('Value invalid from exif data {} (file:{})'.format(
                     d, obj.path))
        return None
    return date


def get_generic_metadata_time(obj: file_attribute) -> (datetime, None):
    """
    Returns the datetime from the metadata creation date
    @params:
        obj - Required : object to consider (file_atribute)
    @returns:
        (datetime) metadata creation date or None if invalid
    """
    assert isinstance(obj, file_attribute)

    parser = createParser(obj.path)
    data = extractMetadata(parser)
    if data is None:
        return None

    metadata = None
    for line in data.exportPlaintext():
        if 'Creation date' in line:
            metadata = line

    # String to datetime
    date = None
    if metadata is not None:
        date = datetime_from_string(metadata)
    return date


def get_metadata(obj: file_attribute) -> (str, None):
    """
    Returns datetime from file metadata (exif or not)
    @params:
        obj - Required : object to consider (file_atribute)
    @returns:
        (datetime) metadata creation date or None if invalid
    """
    assert isinstance(obj, file_attribute)
    if not os.path.isfile(obj.path):
        return None

    metadata = None
    if obj.extension in EXIF_EXT:
        metadata = get_exif_time(obj)
    elif obj.extension in SUPPORTED_EXT:
        metadata = get_generic_metadata_time(obj)
    else:
        logger.debug('Not supported extension {} for \'{}\''.format(
                     obj.extension, obj.path))
    return metadata


def decide_time(obj: file_attribute) -> (datetime, None):
    """
    Decides which datetime to apply to the file using gathered information
    @params:
        obj - Required : object to consider (file_atribute)
    @returns:
        (datetime) decided date or None if un-decided
    """
    assert isinstance(obj, file_attribute)
    assert (obj.file_stamp is None or
            isinstance(obj.file_stamp, datetime))
    assert (obj.metadata_stamp is None or
            isinstance(obj.metadata_stamp, datetime))
    assert (obj.parent_stamp is None or
            isinstance(obj.parent_stamp, datetime))

    # No extra data, skip
    if not obj.metadata_stamp and not obj.parent_stamp:
        return None

    # Let's gather some differences
    meta_par = MARGIN_DECISION + 1
    meta_file = meta_par
    file_par = meta_par

    if obj.metadata_stamp and obj.parent_stamp:
        meta_par = abs(obj.metadata_stamp - obj.parent_stamp).days
    if obj.metadata_stamp and obj.file_stamp:
        meta_file = abs(obj.metadata_stamp - obj.file_stamp).days
    if obj.file_stamp and obj.parent_stamp:
        file_par = abs(obj.file_stamp - obj.parent_stamp).days

    logger.debug('Got date distance: {}, {}, {} (margin:{})'.format(
                 meta_par, meta_file, file_par, MARGIN_DECISION))

    # Choice logic: we tend to trust the timestamp most,
    # followed by the human who organized the pictures in folders.
    if obj.metadata_stamp:                  # We have metadata
        if meta_par < MARGIN_DECISION:      # Metadata seems trustworthy
            return obj.metadata_stamp
        else:                               # Metadata seems NOT trustworthy
            if file_par < MARGIN_DECISION:  # File seems trustworthy
                return obj.file_stamp
            else:                           # File seems NOT trustworthy
                return obj.parent_stamp
    else:                                   # We DON'T have metadata
        if file_par < MARGIN_DECISION:      # File seems trustworthy
            return obj.file_stamp
        else:                               # File seems NOT trustworthy
            return obj.parent_stamp
    return None                             # Couldn't decide on anything


def save_file_attribute_list_to_file(data: list, output: str,
                                     interract: bool = True) -> bool:
    """
    Save the data and decision to a file (.json or .csv)
    @params:
        data      - Required : list of file atribute to save (list)
        output    - Required : path of the file to save the output to (str)
        interract - Optional : should interract with the user or not (bool)
    @returns:
        (bool) status of the save (success = True, failure = False)
    """
    assert isinstance(data, list)
    assert len(data) > 0
    assert isinstance(output, str)

    out_file = os.path.abspath(output)
    if os.path.exists(out_file):
        if interract:
            if not query_yes_no('File {} exists, overwrite?'.format(out_file)):
                return False
        else:
            raise IOError('File {} already exists'.format(out_file))

    # Manually serialize
    json_cpy = []
    for d in data:
        assert isinstance(d, file_attribute)
        json_cpy.append(
            {'path': d.path,
             'action': d.action,
             'decided': (d.decided_stamp.strftime("%Y-%m-%d %H:%M:%S")
                         if d.decided_stamp else None),
             'file': (d.file_stamp.strftime("%Y-%m-%d %H:%M:%S")
                      if d.file_stamp else None),
             'metadata': (d.metadata_stamp.strftime("%Y-%m-%d %H:%M:%S")
                          if d.metadata_stamp else None),
             'parent': (d.parent_stamp.strftime("%Y-%m-%d %H:%M:%S")
                        if d.parent_stamp else None),
             'type': d.type})

    if output.endswith('.json'):
        with open(out_file, 'w', newline='') as fout:
            json.dump(json_cpy, fout)
    elif output.endswith('.csv'):
        keys = json_cpy[0].keys()
        with open(out_file, 'w', newline='') as fout:
            dict_writer = csv.DictWriter(fout, keys)
            dict_writer.writeheader()
            dict_writer.writerows(json_cpy)
    else:
        logger.error('Not recognized file extension for {}'.format(output))
        return False

    logger.info('Created data look fine, wrote to file {}'.format(out_file))
    return True


def execute_plan(plan: list):
    """
    Apply changes to files according to plan. Plan is a list of file_attribute
    @params:
        plan - Required : list of objects to consider (list[file_atribute])
    @returns:
        nothing
    """
    # Check data provided first (you never know)
    assert isinstance(plan, list)
    for p in plan:
        assert isinstance(p, file_attribute)
        if p.action != Action.SKIP:
            assert p.decided_stamp

    # Arriving here didn't trigger any assert, plan seems OK
    total = len(plan)
    cnt = 0
    for p in plan:
        # Print progress
        cnt += 1
        print_progressbar(cnt, total, prefix='3. Apply changes')

        if not isinstance(p.decided_stamp, datetime):
            continue

        if p.action == Action.CHANGE_ALL:
            align_exif_date_with_decided(p)
            align_file_date_with_decided(p)
        elif p.action == Action.CHANGE_METADATA:
            align_exif_date_with_decided(p)
        elif p.action == Action.CHANGE_FILESTAMP:
            align_file_date_with_decided(p)
    return


def touch_recur(root_path: str, output: str = None, execute: bool = False,
                change_filestamp: bool = False, change_exif: bool = False):
    """
    Recursively touch files with the parent folder's date or using metadata
    @params:
        root_path           - Required  : parent folder to process (str)
        output              - Required  : path to the logfile (str)
        execute             - Optional  : do things, not simulations (bool)
        change_filestamp    - Optional  : change OS file creation date (bool)
        change_exif         - Optional  : change exif creation date (bool)
    @returns:
        nothing
    """
    assert isinstance(root_path, str)
    assert output is None or isinstance(output, str)
    assert os.path.isdir(root_path)

    path_obj = get_objects_from_path(root_path)
    cnt_dir = len([o for o in path_obj if o.type == 'dir'])
    total = len(path_obj)
    logger.info('Found {} objects: {} dirs and {} files'.format(
                total, cnt_dir, total - cnt_dir))

    cnt = 0
    for o in path_obj:
        cnt += 1
        print_progressbar(cnt, total, prefix='2. Determine changes')

        # Let's consider that we skip by default
        o.action = Action.SKIP

        # Get different time possibilities
        o.file_stamp = get_object_time(o)
        o.metadata_stamp = get_metadata(o)
        o.parent_stamp = get_parent_time(o)

        # Decide what to do
        o.decided_stamp = decide_time(o)
        logger.debug('Decide returned {}'.format(type(o.decided_stamp)))

        # If we decided on a new timestamp check what to do
        if o.decided_stamp and change_filestamp and o.extension in EXIF_EXT:
            # If file timestamp difference is higher than x days, change it
            file_diff = abs(o.decided_stamp - o.file_stamp).days
            if file_diff > MARGIN_TOUCH:
                o.action = Action.CHANGE_FILESTAMP
                logger.debug('CHANGE file {} to {} (f:{}, m:{}, p:{})'.format(
                            o.path, o.decided_stamp, o.file_stamp,
                            o.metadata_stamp, o.parent_stamp))

        if o.decided_stamp and change_exif and o.extension in EXIF_EXT:
            # If exif timestamp difference is higher than x days, change it
            try:
                exif_diff = abs(o.decided_stamp - o.metadata_stamp).days
            except TypeError:   # No exif data there, add some
                exif_diff = MARGIN_EXIF + 1

            if exif_diff > MARGIN_EXIF:
                if not o.action == Action.CHANGE_FILESTAMP:
                    o.action = Action.CHANGE_METADATA
                else:
                    o.action = Action.CHANGE_ALL
                logger.debug('CHANGE meta {} to {} (f:{}, m:{}, p:{})'.format(
                            o.path, o.decided_stamp, o.file_stamp,
                            o.metadata_stamp, o.parent_stamp))

    # Print some stats
    to_change_touch = len([o for o in path_obj if (
                                o.action == Action.CHANGE_FILESTAMP or
                                o.action == Action.CHANGE_ALL)])
    to_change_meta = len([o for o in path_obj if (
                            o.action == Action.CHANGE_METADATA or
                            o.action == Action.CHANGE_ALL)])
    logger.info('Decided to touch {} files out of {} ({}%)'.format(
                to_change_touch, total, int(to_change_touch / total * 100)))
    logger.info('Decided to change exif for {} files out of {} ({}%)'.format(
                to_change_meta, total, int(to_change_meta / total * 100)))

    # Save to file for review
    if output:
        file_export = save_file_attribute_list_to_file(path_obj,
                                                       output,
                                                       interract=True)

    if execute:
        if not output:
            logger.error('You have not exported the change list. Abort')
        elif output and not file_export:
            logger.error('Change list failed to save. Can\'t review. Abort')
        else:
            if query_yes_no('About to change stuff, do you want to proceed?'):
                if query_yes_no('Did you review {} carefully?'.format(output)):
                    if query_yes_no('Are you really REALLY sure to proceed?'):
                        if query_yes_no('Last ultimatum, still proceed?'):
                            execute_plan(path_obj)
    return


if __name__ == '__main__':
    desc = 'File date correction utility'

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(type=str,
                        default='', dest='root_path',
                        help='Path to process')
    parser.add_argument(type=str,
                        default=None, dest='output',
                        help='Write decision to a json/csv file for review')
    parser.add_argument('-e', '--execute', action='store_true',
                        default=False, dest='execute',
                        help='Execute the changes instead of simulating')
    parser.add_argument('--change_metadata', action='store_true',
                        default=False, dest='change_metadata',
                        help='Change metadata creation date')
    parser.add_argument('--change_timestamp', action='store_true',
                        default=False, dest='change_timestamp',
                        help='Change file creation date')

    args = parser.parse_args()

    # Do stuff
    touch_recur(args.root_path, args.output,
                execute=args.execute, change_filestamp=args.change_timestamp,
                change_exif=args.change_metadata)
    logger.info('Processing done !')
    exit()
