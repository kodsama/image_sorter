#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
""" Select if images are worth keeping or not """

import argparse
from image_sorter import image_sorter as ims

def run(kwargs: dict):
    """ Load images, loops through them and  """
    i = ims.ImageSorter(**kwargs)
    i.run()
    i.print()

if __name__ == '__main__':
    DEF_BF = 100.0

    arg = argparse.ArgumentParser(description='Select if images are worth keeping or not')
    arg.add_argument('path', metavar='PATH',
                    help='Path to images to treat')
    arg.add_argument('-r', '--recursive', action="store_true", default=False,
                    help='Should we treat the subfolders ?')
    arg.add_argument('-rm', '--remove', action="store_true", default=False,
                    help='Should we remove the unworthy images ?')
    arg.add_argument('-ns', '--no_sim', action="store_true", default=False,
                    help='Should simulate and not do anything')
    arg.add_argument('-bt', '--blur_thr', type=float, default=DEF_BF,
                    help='Focus value to be considered as blurry')
    args = vars(arg.parse_args())
    run(args)