#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
import sys
from setuptools import setup, find_packages

if sys.version_info[0] < 3:
    sys.exit("Sorry, Python 2 is not supported")

__version__ = '0.0.1'

NAME = 'image_sorter'
PACKAGES = find_packages(exclude=['test', 'test_*.*'])

DESCRIPTION = """
A simple library to sort pictures in folder and subfolders
in order to list (and/or delete) bad pictures.
"""

setup(
    name=NAME,
    version=__version__,
    packages=PACKAGES,
    install_requires=[
        'opencv-python>=4.6.0.66',
        'termcolor>=1.1.0',
        'hachoir>=3.1.3',
        'piexif>=1.1.3',
        'coloredlogs>=15.0.1',
        'pandas>=1.4.3',
        ],
    extras_require={
        'dev': [
            'pytest>=7.1.2',
            'coverage>=6.4.3'
            ]
        },
    description=DESCRIPTION,
    long_description=DESCRIPTION,
    package_data={
        '': ['*.md']
        },
    author="Alexandre Martins (a.k.a Kodsama)",
    author_email="kodsama@protonmail.com",
    license="Proprietary",
    keywords="opencv, image quality",
    url="https://gitlab.com/Kodsama/ImageSorter",
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Environment :: Other Environment',
        'Operating System :: OS Independent',
        'License :: Other/Proprietary License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Topic :: Utilities',
    ],
    zip_safe=True
)
